const micro = require('./src/index');

async function main() {

    try {
    
        const db = await micro.SyncDB();

        if (db.statusCode !== 200) throw db.message;

        await micro.run();
    
    } catch(err) {

        console.log(err);
    }
}

main();