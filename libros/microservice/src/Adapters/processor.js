const Service = require('../Services/');
const { InternalError } = require('../settings');

const { queueView, queueCreate, queueUpdate, queueDelete, queueFindone } = require('./index');

async function View(job, done) {
        
    try {
        
        const { } = job.data;

        const { statusCode, data, message } = await Service.View({  });
        
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueView" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Create(job, done) {
    
    try {

        const { title, image } = job.data;

        const { statusCode, data, message } = await Service.Create({ title, image });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueCreate" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Update(job, done) {
        
    try {

        const { title, category, seccions, id } = job.data;

        const { statusCode, data, message } = await Service.Update({ title, category, seccions, id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueUpdate" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Delete(job, done) {
        
    try {

        const { id } = job.data;

        const { statusCode, data, message } = await Service.Delete({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueDelete" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Findone(job, done) {
        
    try {

        const { title } = job.data;
        
        const { statusCode, data, message } = await Service.FindOne({ title });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador FindOne" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function run() {

    try {
        
        console.log("Vamos a inicializar worker");

        queueView.process(View);
        queueCreate.process(Create);
        queueUpdate.process(Update);
        queueDelete.process(Delete);
        queueFindone.process(Findone);

    } catch (err) {

        console.log(err);
    }
}

module.exports = {
    View,
    Create,
    Update,
    Delete,
    Findone,
    run,
}