const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');
const { name } = require('../../package.json');

const Model = sequelize.define(name, {
    title: { type: DataTypes.STRING },
    category: { type: DataTypes.STRING },
    seccions: { type: DataTypes.STRING },
    image: { type: DataTypes.STRING },
});

const SyncDB = async () => {
    
    try {

        console.log("Vamos a inicializar la base de datos");

        await Model.sync({ logging: false, force: true });

        console.log("Base de datos inicializada");

        return { statusCode: 200, data: 'ok' }

    } catch (err) {

        console.log(err);

        return { statusCode: 500, message: err.toString() }
    }
}

module.exports = { SyncDB, Model };