## API de libros

Esta es una api para interactuar con los libros registrados en la bibloteca

```js

const apiLibros = require('libros');

socket.on('req:libros:view', async ({}) => {

    try {

        const { statusCode, data, message } = await apiLibros.View({});

        return io.to(socket.id).emit('res:libros:view', { statusCode, data, message });

    } catch (err) {

        console.log(err);
    }
});

socket.on('req:libros:create', async ({ title }) => {
    
    try {

        const { statusCode, data, message } = await apiLibros.Create({ title });

        return io.to(socket.id).emit('res:libros:create', { statusCode, data, message });

    } catch (err) {

        console.log(err);
    }
});

socket.on('req:libros:update', async ({ title }) => {

    try {

        const { statusCode, data, message } = await apiLibros.Update({ title });

        return io.to(socket.id).emit('res:libros:update', { statusCode, data, message });

    } catch (err) {

        console.log(err);
    }
});

socket.on('req:libros:delete', async ({ id }) => {

    try {

        const { statusCode, data, message } = await apiLibros.Delete({ id });

        return io.to(socket.id).emit('res:libros:delete', { statusCode, data, message });

    } catch (err) {

        console.log(err);
    }
});

socket.on('req:libros:findOne', async ({ title }) => {

    try {

        const { statusCode, data, message } = await apiLibros.FindOne({ title });

        return io.to(socket.id).emit('res:libros:findOne', { statusCode, data, message });

    } catch (err) {

        console.log(err);
    }
});
```

Solo se puede registrar libros de categoria A