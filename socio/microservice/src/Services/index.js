const Controllers = require('../Controllers/');
const { InternalError } = require('../settings');

async function Create({ phone, name }) {
    
    try {

        const { statusCode, data, message } = await Controllers.Create({ phone, name });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service Create" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function Delete({ id }) {
    
    try {

        const findOne = await Controllers.FindOne({ whuere: { id } });

        if (findOne.statusCode !== 200) {

            const response = {
                400: { statusCode: 400, message: "No existe el usuario a eliminar", },
                500: { statusCode: 500, message: InternalError },
            }

            return response[findOne.statusCode];
        }

        const del = await Controllers.Delete({ where: { id } });

        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data };

        return { statusCode: 400, data: InternalError };
    
    } catch(err) {

        console.log({step:"service Delete" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function Update({ id, name, phone, email, age }) {
    
    try {

        const { statusCode, data, message } = await Controllers.Update({ id, name, phone, email, age });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service Update" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function FindOne({ id }) {
    
    try {

        const { statusCode, data, message } = await Controllers.FindOne({ where: { id } });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service FindOne" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function View({ enable }) {
    
    try {
        
        const { statusCode, data, message } = await Controllers.View({ where: enable });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service View" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}

async function Enable({ id }) {
    
    try {

        const { statusCode, data, message } = await Controllers.Enable({ where: { id } });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service Enable" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}


async function Disable({ id }) {
    
    try {

        const { statusCode, data, message } = await Controllers.Disable({ id });

        return { statusCode, data, message };
    
    } catch(err) {

        console.log({step:"service Disable" , err});

        return {
            statusCode: 500,
            data: null,
            message: err.toString(),
        };
    }
}


module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
    Enable,
    Disable,
}