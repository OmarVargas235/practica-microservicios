const Service = require('../Services/');
const { InternalError } = require('../settings');

const { queueView, queueCreate, queueUpdate, queueDelete, queueFindone, queueEnable, queueDisable } = require('./index');

async function View(job, done) {
        
    try {
        
        const { enable } = job.data;

        const { statusCode, data, message } = await Service.View({ enable });
        
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueView" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Create(job, done) {
    
    try {

        const { phone, name } = job.data;

        const { statusCode, data, message } = await Service.Create({ phone, name });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueCreate" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Update(job, done) {
        
    try {

        const { id, name, phone, email, age } = job.data;

        const { statusCode, data, message } = await Service.Update({ id, name, phone, email, age });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueUpdate" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Delete(job, done) {
        
    try {

        const { id } = job.data;

        const { statusCode, data, message } = await Service.Delete({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueDelete" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Findone(job, done) {
        
    try {

        const { id } = job.data;
        
        const { statusCode, data, message } = await Service.FindOne({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador FindOne" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Enable(job, done) {
        
    try {

        const { id } = job.data;
        
        const { statusCode, data, message } = await Service.Enable({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador Enable" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Disable(job, done) {
        
    try {

        const { id } = job.data;
        
        const { statusCode, data, message } = await Service.Disable({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador Disable" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function run() {

    try {
        
        console.log("Vamos a inicializar worker");

        queueView.process(View);
        queueCreate.process(Create);
        queueUpdate.process(Update);
        queueDelete.process(Delete);
        queueFindone.process(Findone);
        queueEnable.process(Enable);
        queueDisable.process(Disable);

    } catch (err) {

        console.log(err);
    }
}

module.exports = {
    View,
    Create,
    Update,
    Delete,
    Findone,
    Enable,
    Disable,
    run,
}