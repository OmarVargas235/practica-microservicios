const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');
const { name } = require('../../package.json');

const Model = sequelize.define(name, {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    email: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING },
    enable: { type: DataTypes.BOOLEAN },
}, { freezeTableName: true });

const SyncDB = async () => {
    
    try {

        console.log("Vamos a inicializar la base de datos");

        await Model.sync({ logging: false, force: true });

        console.log("Base de datos inicializada");

        return { statusCode: 200, data: 'ok' }

    } catch (err) {

        console.log(err);

        return { statusCode: 500, message: err.toString() }
    }
}

module.exports = { SyncDB, Model };