const { name } = require('./package.json');
const bull = require('bull');

const redis = { host: '192.168.88.103', port: '6379' };

const opts =  { redis: { host: redis.host, port: redis.port } };

const queueCreate= bull(name+":create", opts);
const queueDelete = bull(name+":delete", opts);
const queueUpdate = bull(name+":update", opts);
const queueFindone = bull(name+":findOne", opts);
const queueView = bull(name+":view", opts);
const queueEnable = bull(name+":enable", opts);
const queueDisable = bull(name+":disable", opts);

async function Create({ phone, name }) {

    try {

        const job = await queueCreate.add({ phone, name });
    
        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function Delete({ id }) {

    try {

        const job = await queueDelete.add({ id });
    
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function FindOne({ id }) {

    try {

        const job = await queueFindone.add({ id });
    
        const { statusCode, data, message } = await job.finished();
        
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function Update({ email, name, age, phone, id }) {

    try {

        const job = await queueUpdate.add({ email, name, age, phone, id });
    
        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function View({ enable }) {
   
    try {
        
        const job = await queueView.add({ enable });
        
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function Enable({ id }) {
   
    try {
        
        const job = await queueEnable.add({ id });
        
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }
}

async function Disable({ id }) {
   
    try {
        
        const job = await queueDisable.add({ id });
        
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function main() {

    // await Create({name: 'omar', edad: 22, color: 'green'});
    // await Delete({id: 10});
    // await Update({ name: "jaimes", age: 57, color: 'pink', id: 7 });
    await FindOne({ id: 16 });
    // await View({});
}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
    Enable,
    Disable,
}