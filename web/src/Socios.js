import React, { useEffect } from 'react';
import styled from 'styled-components';

import { socket } from './ws';
import { useGetData } from './customHooks/useGetData';
import { deleteIcon } from './helper';

const Container = styled.div`
    width: 300px;
    max-width: 300px;
`;

const ContainerBody = styled.div`
    overflow-y: scroll;
    height: ${({heightSlider}) => heightSlider - 190}px;
`;

const Socio = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 25px;
    position: relative;
`;

const Body = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const Button = styled.button`
    background-color: #f44336;
    padding: 12px 20px;
    opacity: .8;
    border-radius: 15px;
    color: white;
    border: none;
    margin: 10px 10px 20px;
    width: 100%;
    cursor: pointer;
`;

const Icon = styled.img`
    height: 35px;
    width: 35px;
    cursor: pointer;
    margin-left: 10px;
`;

const FeedBack = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

const Name = styled.p`
`;

const Phone = styled.p`
`;

const Email = styled.p`
`;

const Enable = styled.p`
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: ${({ enable }) => enable ? 'green' : 'red'};
    opacity: .5;
    cursor: pointer;
`;

const Socios = ({ heightSlider }) => {

    const [socios, setSocios] = useGetData({eventOn: 'res:socio:view', eventEmmit: 'req:socio:view'});

    useEffect(() => {

        socket.on('res:socio:view', ({ statusCode, data }) => statusCode === 200 && setSocios(data));

    }, [setSocios]);

    const handleCreate = () => {

        socket.emit('req:socio:create', { phone: '041247297537', name: 'Omar' }, ({statusCode, data}) => {
            
            statusCode === 200 && setSocios(data);
        });
    }

    const handleDelete = id => socket.emit('req:socio:delete', {id}, ({ statusCode, data }) => statusCode === 200 && setSocios(data));

    const handleChangeEnable = (id, isEnable) => {

        isEnable
        ? socket.emit('req:socio:disable', {id}, ({ statusCode, data }) => statusCode === 200 && setSocios(data))
        : socket.emit('req:socio:enable', {id}, ({ statusCode, data }) => statusCode === 200 && setSocios(data));
    }
    
    return (
        <Container>
            <Button onClick={handleCreate}>Crear</Button>

            <ContainerBody heightSlider={heightSlider}>
                {
                    socios.map((socio, index) => (
                        <Socio key={index}>
                            <Body>
                                <Name>{socio.name} {socio.id}</Name>
                                <Phone>{socio.phone}</Phone>
                            </Body>
                            
                            <Body>
                                <Email>{socio.email}</Email>

                                <FeedBack>
                                    <Enable
                                        enable={socio.enable}
                                        onClick={() => handleChangeEnable(socio.id, socio.enable)}
                                    >{socio.enable}</Enable>
                                    <Icon
                                        src={deleteIcon}
                                        onClick={() => handleDelete(socio.id)}
                                    />
                                </FeedBack>
                            </Body>
                        </Socio>
                    ))
                }
            </ContainerBody>
        </Container>
    );
}

export default Socios;