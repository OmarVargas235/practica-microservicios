import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import Card from 'card';

import Socios from './Socios';
import Libros from './Libros';
import Pagos from './Pagos';

const Container = styled.div`
    display: flex;
    min-height: 100vh;
    margin: 0;
`;

const Slider = styled.div`
    width: 35%;
    min-width: 350px;
`;

const Body = styled.div`
    width: 65%;
    min-width: 350px;
    background-color: lightgray;
`;

const img = 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png';

const description = 'Get started quickly. Start developing from a single .js file or generate a project skeleton.Covers the whole development cycle. Development tools, testing and production.';


function App() {

    const sliderReft = useRef(null);
    const [heightSlider, setHeightSlider] = useState(0);

    useEffect(() => {

        const { current } = sliderReft;

        if (!current) return;
        
        setHeightSlider(current.clientHeight);

    }, [sliderReft]);
    
    return (
        <Container>
            {/* <Slider ref={sliderReft}>
                <Socios heightSlider={heightSlider} />
                <Pagos />
            </Slider>

            <Body>
                <Libros />
            </Body> */}

            <Card
                image={img}
                title="Centro de entrenamiento"
                description={description}
                labelBtn="Instalar App"
            />
        </Container>
    );
}

export default App;
