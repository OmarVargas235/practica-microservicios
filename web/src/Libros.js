import React from 'react';
import styled from 'styled-components';

import { deleteIcon, imgLibro } from './helper';
import { useGetData } from './customHooks/useGetData';
import { socket } from './ws';

const Container = styled.div`
    position: relative;
    justify-content: space-evenly;
    display: flex;
    flex-wrap: wrap;
    width: 100%;
`;

const Libro = styled.div`
    background-color: #303536;
    width: 200px;
    height: 100%;
    margin-bottom: 15px;
    border-radius: 10px;
    padding: 2px;
    position: relative;
`;

const Portada = styled.img`
    height: 150px;
    width: 100%;
`;

const Icon = styled.img`
    height: 35px;
    width: 35px;
    position: absolute;
    top: 0;
    right: 0;
    cursor: pointer;
`;

const Title = styled.p`
    color: white;
    text-align: center;
`;

const Button = styled.button`
    background-color: #036e00;
    padding: 12px 20px;
    opacity: .8;
    border-radius: 15px;
    color: white;
    border: none;
    margin: 10px 10px 20px;
    width: 100%;
    cursor: pointer;
`;

const Item = ({ libro, setLibros }) => {

    const handleDelete = id => socket.emit('req:libros:delete', {id}, ({ statusCode, data }) => statusCode === 200 && setLibros(data));

    return (
        <Libro>
            <Portada src={libro.image} />
            <Icon
                src={deleteIcon}
                onClick={() => handleDelete(libro.id)}
            />
            <Title>{libro.title}</Title>
        </Libro>
    );
}

const Libros = () => {
    
    const [libros, setLibros] = useGetData({eventOn: 'res:libros:view', eventEmmit: 'req:libros:view'});

    const handleCreate = () => {
        
        socket.emit('req:libros:create', { title: '1984', image: imgLibro }, ({ statusCode, data }) => {

            statusCode === 200 && setLibros(data);
        });
    }

    return (
        <Container>
            <Button onClick={handleCreate}>Crear</Button>

            {
                libros.map((libro, index) => (
                    <Item
                        libro={libro}
                        key={index}
                        setLibros={setLibros}
                    />
                ))
            }
        </Container>
    );
}

export default Libros;