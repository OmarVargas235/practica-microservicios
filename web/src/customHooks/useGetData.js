import { useState, useEffect } from 'react';

import { socket } from '../ws';

export const useGetData = ({ eventOn, eventEmmit, sendData={} }) => {

    const [data, setData] = useState([]);

    useEffect(() => {
      
        socket.on(eventOn, ({ statusCode, data }) => {

            // console.log('req:libros:view', { statusCode, data });

            statusCode === 200 && setData(data);
        });

        socket.emit(eventEmmit, sendData);

    }, [socket]);

    return [data, setData];
}