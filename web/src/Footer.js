import React, { useState, useEffect } from 'react';

const Footer = () => {

    const [id, setId] = useState();

    useEffect(() => setTimeout(() => setId(socket.id), 200), [socket]);

    return (
        <p>{id ? `Estas en linea ${id}` : 'Fuera de linea'}</p>
    );
}

export default Footer;