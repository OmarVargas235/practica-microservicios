import React, { useState } from 'react';
import styled from 'styled-components';

import { deleteIcon } from './helper';
import { useGetData } from './customHooks/useGetData';
import { socket } from './ws';

const Container = styled.div`
    width: 300px;
    max-width: 300px;
    margin-top: 10px;
`;

const ContainerBody = styled.div`
    overflow-y: scroll;
    height: 250px;
`;

const Socio = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 25px;
    position: relative;
`;

const Body = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

const Button = styled.button`
    background-color: #f44336;
    padding: 12px 20px;
    opacity: .8;
    border-radius: 15px;
    color: white;
    border: none;
    margin: 10px 10px 20px;
    width: 100%;
    cursor: pointer;
`;

const Icon = styled.img`
    height: 35px;
    width: 35px;
    cursor: pointer;
    margin-left: 10px;
`;

const Input = styled.input`
    border-radius: 10px;
    width: 90%;
    margin-left: 10px;
`;

const Name = styled.p``;

const Pagos = () => {

    const [pagos, setPagos] = useGetData({eventOn: 'res:pagos:view', eventEmmit: 'req:pagos:view'});

    const [value, setValue] = useState('');

    const handleCreate = () => {

        value > 0
        && socket.emit('req:pagos:create', { socio: value, amount: 'Omar' }, ({statusCode, data}) => {
            
            statusCode === 200 && setPagos(data);
        });

        setValue('');
    }

    const handleDelete = id => socket.emit('req:pagos:delete', {id}, ({ statusCode, data }) => statusCode === 200 && setPagos(data));

    const handleChangeInput = e => setValue(e.target.value);

    return (
        <Container>

            <Input onChange={handleChangeInput} value={value} />

            <Button onClick={handleCreate}>Aplicar pago para el socio {value}</Button>

            <ContainerBody>
                {
                    pagos.map((pago, index) => (
                        <Socio key={index}>
                            <Body>
                                <Name>Cupon de Pago: {pago.id}</Name>
                                <Name>{pago.socio}</Name>
                            </Body>
                            
                            <Body>
                                <Name>{pago.createdAt}</Name>

                                <Icon
                                    src={deleteIcon}
                                    onClick={() => handleDelete(pago.id)}
                                />
                            </Body>
                        </Socio>
                    ))
                }
            </ContainerBody>
        </Container>
    );
}

export default Pagos;