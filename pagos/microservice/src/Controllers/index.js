const { Model } = require('../Models');

async function Create({ socio, amount }) {

    try {

        const instance = await Model.create(
            { socio, amount },
            { fields: ['socio', 'amount'], logging: false }
        );
        
        return { statusCode: 200, data: instance.toJSON() };
         
    } catch (err) {

        console.log({ step: 'controller Create', error: err.toString() });

        return { statusCode: 500, message: err.toString() };
    }
}

async function Delete({ where={} }) {

    try {
        
        await Model.destroy({ where, logging: false });
        
        return { statusCode: 200, data: "OK" };

    } catch (err) {

        console.log({ step: 'Delete Create', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

async function FindOne({ where={} }) {
    
    try {

        const instance = await Model.findOne({ where, logging: false });
        
        return instance
            ? { statusCode: 200, data: instance.toJSON() }
            : { statusCode: 400, message: 'No existe el usuario' };

    } catch (err) {

        console.log({ step: 'controller FindOne', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

async function View({ where={} }) {

    try {

        const instances = await Model.findAll({ where, logging: false });
        
        return { statusCode: 200, data: instances };

    } catch (err) {

        console.log({ step: 'controller View', error: err.toString() });

        return { statusCode: 500, message: err.toString() }
    }
}

module.exports = {
    Create,
    Delete,
    FindOne,
    View,
}