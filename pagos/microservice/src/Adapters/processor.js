const Service = require('../Services/');
const { InternalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueFindone } = require('./index');

async function View(job, done) {
        
    try {
        
        const { } = job.data;

        const { statusCode, data, message } = await Service.View({  });
        
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueView" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Create(job, done) {
    
    try {

        const { socio, amount } = job.data;

        const { statusCode, data, message } = await Service.Create({ socio, amount });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueCreate" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Delete(job, done) {
        
    try {

        const { id } = job.data;

        const { statusCode, data, message } = await Service.Delete({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador queueDelete" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function Findone(job, done) {
        
    try {

        const { id } = job.data;
        
        const { statusCode, data, message } = await Service.FindOne({ id });
    
        done(null, { statusCode, data, message });

    } catch(err) {

        console.log({step:"Adaptador FindOne" , err});

        done(null, { statusCode: 500, message: InternalError });
    }
};

async function run() {

    try {
        
        console.log("Vamos a inicializar worker");

        queueView.process(View);
        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueFindone.process(Findone);

    } catch (err) {

        console.log(err);
    }
}

module.exports = {
    View,
    Create,
    Delete,
    Findone,
    run,
}