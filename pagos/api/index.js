const bull = require('bull');
const { name } = require('./package.json');

const redis = { host: '192.168.88.103', port: '6379' };

const opts =  { redis: { host: redis.host, port: redis.port } };

const queueCreate= bull(name+":create", opts);
const queueDelete = bull(name+":delete", opts);
const queueFindone = bull(name+":findOne", opts);
const queueView = bull(name+":view", opts);

async function Create({ socio, amount }) {

    try {

        const job = await queueCreate.add({ socio, amount });
    
        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function Delete({ id }) {

    try {

        const job = await queueDelete.add({ id });
    
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function FindOne({ id }) {

    try {

        const job = await queueFindone.add({ id });
    
        const { statusCode, data, message } = await job.finished();
        
        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function View({}) {
   
    try {
        
        const job = await queueView.add({});
        
        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message };

    } catch (err) {

        console.log(err);
    }

}

async function main() {

    // await Create({name: 'omar', edad: 22, color: 'green'});
    // await Delete({id: 10});
    // await Update({ name: "jaimes", age: 57, color: 'pink', id: 7 });
    await FindOne({ id: 16 });
    // await View({});
}

module.exports = {
    Create,
    Delete,
    FindOne,
    View,
}