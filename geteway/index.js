const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const apiLibros = require('libros');
const apiSocios = require('socio');
const apipagos = require('pagos');
const { callbackify } = require('util');

const app = express();
const server = http.createServer( app );

const io = new Server(server);

server.listen(80, () => {

    console.log("Corriendo servidor en el puerto 80");

    io.on('connection', (socket) => {

        console.log("new connection", socket.id);

        // Libros
        socket.on('req:libros:view', async () => {

            try {

                // console.log('req:libros:view');

                const { statusCode, data, message } = await apiLibros.View({});

                return io.to(socket.id).emit('res:libros:view', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:libros:create', async ({ title, image }, callback) => {
            
            try {
                
                // console.log('req:libros:create', { title, image });

                const { statusCode, data, message } = await apiLibros.Create({ title, image });
                const view = await apiLibros.View({});

                callback(view);

                return io.to(socket.id).emit('res:libros:create', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:libros:update', async ({ title }) => {

            try {
                
                // console.log('req:libros:update', { title });

                const { statusCode, data, message } = await apiLibros.Update({ title });

                return io.to(socket.id).emit('res:libros:update', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:libros:delete', async ({ id }, callback) => {

            try {
                
                console.log('req:libros:delete', { id });

                const { statusCode, data, message } = await apiLibros.Delete({ id });
                const view = await apiLibros.View({});

                callback(view);

                return io.to(socket.id).emit('res:libros:delete', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:libros:findOne', async ({ title }) => {

            try {
                
                console.log('req:libros:findOne', { title });

                const { statusCode, data, message } = await apiLibros.FindOne({ title });

                return io.to(socket.id).emit('res:libros:findOne', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        // Pagos
        socket.on('req:pagos:view', async ({}) => {

            try {

                // console.log('req:pagos:view');

                const { statusCode, data, message } = await apipagos.View({});

                return io.to(socket.id).emit('res:pagos:view', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:pagos:create', async ({ socio, amount }, callback) => {
            
            try {
                
                // console.log('req:pagos:create', { socio, amount });

                const { statusCode, data, message } = await apipagos.Create({ socio, amount });
                const view = await apipagos.View({});
                const viewSocios = await apiSocios.View({});

                callback(view);

                const dataView = { statusCode: viewSocios.statusCode, data: viewSocios.data, message: viewSocios.message };

                io.to(socket.id).emit('res:socio:view', dataView);
                return io.to(socket.id).emit('res:pagos:create', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:pagos:delete', async ({ id }, callback) => {

            try {
                
                // console.log('req:microservice:delete', { id });

                const { statusCode, data, message } = await apipagos.Delete({ id });
                const view = await apipagos.View({});

                callback(view);

                return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:pagos:findOne', async ({ id }) => {

            try {
                
                console.log('req:microservice:findOne', { id });

                const { statusCode, data, message } = await apipagos.FindOne({ id });

                return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        // Socios
        socket.on('req:socio:view', async ({ enable }) => {

            try {

                // console.log('req:socio:view', { enable });

                const { statusCode, data, message } = await apiSocios.View({ enable });

                return io.to(socket.id).emit('res:socio:view', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:socio:create', async ({ phone, name }, callback) => {
            
            try {
                
                // console.log('req:socio:create', { phone, name });

                const { statusCode, data, message } = await apiSocios.Create({ phone, name });
                
                const view = await apiSocios.View({});
                
                callback(view);

                return io.to(socket.id).emit('res:socio:create', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:socio:update', async ({ email, name, age, phone, id }) => {

            try {
                
                console.log('req:socio:update', { email, name, age, phone, id });

                const { statusCode, data, message } = await apiSocios.Update({ email, name, age, phone, id });

                return io.to(socket.id).emit('res:socio:update', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:socio:delete', async ({ id }, callback) => {

            try {
                
                // console.log('req:socio:delete', { id });

                const { statusCode, data, message } = await apiSocios.Delete({ id });
                const view = await apiSocios.View({});

                callback(view);

                return io.to(socket.id).emit('res:socio:delete', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:socio:findOne', async ({ id }) => {

            try {
                
                console.log('req:socio:findOne', { id });

                const { statusCode, data, message } = await apiSocios.FindOne({ id });

                return io.to(socket.id).emit('res:socio:findOne', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:socio:enable', async ({ id }, callback) => {

            try {
                
                // console.log('req:socio:enable', { id });

                const { statusCode, data, message } = await apiSocios.Enable({ id });
                const view = await apiSocios.View({  });

                callback(view);

                return io.to(socket.id).emit('res:socio:enable', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });

        socket.on('req:socio:disable', async ({ id }, callback) => {

            try {
                
                // console.log('req:socio:disable', { id });

                const { statusCode, data, message } = await apiSocios.Disable({ id });
                const view = await apiSocios.View({  });
                
                callback(view);

                return io.to(socket.id).emit('res:socio:disable', { statusCode, data, message });

            } catch (err) {

                console.log(err);
            }
        });
    });
});